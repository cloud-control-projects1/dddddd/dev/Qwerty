package com.company.qwerty;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan
public class QwertyApplication {

    public static void main(String[] args) {
        SpringApplication.run(QwertyApplication.class, args);
    }
}
